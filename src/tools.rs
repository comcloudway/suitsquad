use miniquad as mq;
use std::{
    sync::{
        Arc,
        Mutex
    },
    ops::Deref
};
use crate::{
    CardDrawContext,
    get_context
};

pub fn scree_size() -> (f32, f32) {
    let ctx = get_context();
    ctx.screen_size()
}
pub fn screen_width() -> f32 {
    scree_size().0
}
pub fn screen_height() -> f32 {
    scree_size().1
}

pub fn get_card_scale() -> f32 {
    let ws = screen_width() / 9.0 / CardDrawContext::get_width();
    let hs = screen_height() / 7.0 / CardDrawContext::get_height();

    ws.min(hs)
}

pub fn load_file(path: &str) -> Result<Vec<u8>, mq::fs::Error> {
    let cont = Arc::new(Mutex::new(None));
    let path = path.to_owned();

    {
        let cont = Arc::clone(&cont);
        let p = path.clone();

        mq::fs::load_file(&p, move |bytes| {
            *cont.lock().unwrap() = Some(bytes);
        });
    }

    if let Some(result) = cont.lock().unwrap().deref() {
        if let Ok(bytes) = result {
            return Ok(bytes.to_vec());
        }
    }

    Err(mq::fs::Error::AndroidAssetLoadingError)
}
pub fn load_texture(
    ctx: &mut mq::graphics::Context,
    path: &str,
) -> Option<mq::graphics::Texture> {
    if let Ok(bytes) = load_file(path) {
        let img = image::load_from_memory(&bytes);
        if let Ok(img) = img {
            let img = img.to_rgba8();
            let width = img.width() as u16;
            let height = img.height() as u16;
            let bytes = img.into_raw();

            return Some(
                mq::Texture::from_rgba8(
                    ctx,
                    width,
                    height,
                    &bytes
                )
            );
        }
    }

    None
}
