// templated from
// https://codeberg.org/comcloudway/egui-miniquad-template/src/branch/main/src/main.rs

use miniquad as mq;
use egui_miniquad as egui_mq;
use egui;

mod components;
use components::NamedGroup;
mod tools;
mod game;
use game::{
    Game,
    CardDrawContext,
    card,
    Score
};
use tools::{
    load_texture,
    get_card_scale,
};

static mut CONTEXT: Option<mq::Context> = None;
pub fn get_context() -> &'static mut mq::Context {
    unsafe { CONTEXT.as_mut().unwrap_or_else(|| panic!()) }
}

/// app popups
struct AppPopups {
    /// shows the about this application popup
    /// should include icon and name
    /// and more detailed meta information
    about: bool,
    /// shows the final score from a game
    /// allows to start new game or close application
    /// should display the points gained, lost
    /// should display the calculated score
    score: bool,
    /// shows the settings window
    /// TODO allows to change card colors
    /// TODO allows to change theme
    settings: bool,
    /// shows the stats window
    /// TODO shows a plot with bars outlining the score over time
    /// TODO lists all games
    /// TODO shows rank
    stats: bool
}
impl AppPopups {
    pub fn default() -> AppPopups {
        AppPopups {
            about: false,
            score: false,
            stats: false,
            settings: false
        }
    }
}
/// stored app data
struct App {
    /// label
    name: String,
    /// texture id
    icon: egui::TextureId,
    /// popups that are supported by the app
    popups: AppPopups,
    /// the game being played
    game: Game,
    /// keep stats on top games
    stats: Vec<Score>,
    /// the egui lib
    egui_mq: egui_mq::EguiMq,
}
impl App {
    /// initialises new app context
    pub fn new(ctx: &mut mq::Context) -> Self {
        let icon_path = {
            #[cfg(target_os="android")]
            {
                "icon.png"
            }
            #[cfg(not(target_os="android"))]
            {
                "icon.png"
            }
        };

        let texture = load_texture(
            ctx,
            icon_path,
        ).unwrap();
        let egui_tid = egui::TextureId::User(texture.gl_internal_id() as u64);

        // load themes
        let storage = &mut quad_storage::STORAGE.lock().unwrap();
        if let Some(theme_clubs) = storage.get("theme_clubs") {
            let v = theme_clubs
                .split(',')
                .map(|s|s.parse::<u8>().expect("Failed to load theme"))
                .collect::<Vec<u8>>();
            if v.len() == 3 {
                let r = v.get(0).unwrap();
                let g = v.get(1).unwrap();
                let b = v.get(2).unwrap();

                unsafe {
                    card::CLUBS_COLOR = [*r,*g,*b];
                }
            }
        }
        if let Some(theme_clubs) = storage.get("theme_spades") {
            let v = theme_clubs
                .split(',')
                .map(|s|s.parse::<u8>().expect("Failed to load theme"))
                .collect::<Vec<u8>>();
            if v.len() == 3 {
                let r = v.get(0).unwrap();
                let g = v.get(1).unwrap();
                let b = v.get(2).unwrap();

                unsafe {
                    card::SPADES_COLOR = [*r,*g,*b];
                }
            }
        }
        if let Some(theme_clubs) = storage.get("theme_diamonds") {
            let v = theme_clubs
                .split(',')
                .map(|s|s.parse::<u8>().expect("Failed to load theme"))
                .collect::<Vec<u8>>();
            if v.len() == 3 {
                let r = v.get(0).unwrap();
                let g = v.get(1).unwrap();
                let b = v.get(2).unwrap();

                unsafe {
                    card::DIAMONDS_COLOR = [*r,*g,*b];
                }
            }
        }
        if let Some(theme_clubs) = storage.get("theme_hearts") {
            let v = theme_clubs
                .split(',')
                .map(|s|s.parse::<u8>().expect("Failed to load theme"))
                .collect::<Vec<u8>>();
            if v.len() == 3 {
                let r = v.get(0).unwrap();
                let g = v.get(1).unwrap();
                let b = v.get(2).unwrap();

                unsafe {
                    card::HEARTS_COLOR = [*r,*g,*b];
                }
            }
        }

        // load scoreboard
        let mut stats = Vec::new();
        if let Some(json) = storage.get("stats") {
            if let Ok(dataset) = serde_json::from_str(&json) {
                stats = dataset;
            }
        }

        App {
            egui_mq: egui_mq::EguiMq::new(ctx),
            icon: egui_tid,
            name: String::from("Suitsquad"),
            popups: AppPopups::default(),
            game: Game::new(),
            stats
        }
    }
}
impl mq::EventHandlerFree for App {
    fn update(&mut self) {
        if self.game.is_won() {
            if self.popups.score == false {

                let score = self.game.score.clone();
                let skill = score.calc_skill();

                // attempt to sort score
                let mut index = 0;
                while let Some(item) = self.stats.get(index) {
                    if skill >= item.calc_skill() {
                        break;
                    } else {
                        index+=1;
                    }
                };
                self.stats.insert(index, score);

                // safe stats
                if let Ok(s) = serde_json::to_string(&self.stats) {
                    let storage = &mut quad_storage::STORAGE.lock().unwrap();
                    storage.set("stats", &s);
                }

                // only keep record f top 10
                if self.stats.len()>10 {
                    self.stats.pop();
                }

            }
            self.popups.score = true;
        } else {
            self.popups.score = false;
        }

        self.game.tick();
    }

    fn draw(&mut self) {
        let ctx = get_context();
        ctx.clear(Some((1.,1.,1.,1.)), None, None);
        ctx.begin_default_pass(mq::PassAction::clear_color(0.0, 0.0, 0.0, 1.0));
        ctx.end_render_pass();
        // Run the UI code:
        self.egui_mq.run(ctx, |egui_ctx| {

            // egui stuff
            egui::TopBottomPanel::top("top_panel")
                .show(egui_ctx, |ui| {
                    egui::menu::bar(ui, |ui| {
                        // option to start a new game
                        ui.menu_button("Game", |ui| {
                            if ui.button("New").clicked() {
                                self.game=Game::new();
                            }
                            if ui.button("Settings").clicked() {
                                self.popups.settings=!self.popups.settings;
                            }
                            if ui.button("Stats").clicked() {
                                self.popups.stats=!self.popups.stats;
                            }
                            // option to quit application
                            #[cfg(not(target_arch = "wasm32"))]
                            {
                                if ui.button("Quit").clicked() {
                                    std::process::exit(0);
                                }
                            }
                        });

                        // show about dialog
                        if ui.button("About").clicked() {
                            self.popups.about=!self.popups.about;
                        }

                    });
                });

            egui::CentralPanel::default()
                .show(egui_ctx, |ui| {
                    self.game.draw(ui);

                    #[cfg(target_os = "android")]
                    {
                        // code to access native android functions
                        //if let Some(a) = miniquad::sapp_android::get_native_activity() {
                        // }

                        ui.add_space(10.0);

                        ui.horizontal(|ui| {
                            // option to start a new game
                            if ui.button("New").clicked() {
                                self.game=Game::new();
                            }
                            // option to quit application
                            if ui.button("Quit").clicked() {
                                std::process::exit(0);
                            }
                        });
                    }
                });

            egui::Window::new("You won the game")
                .open(&mut self.popups.score)
                .show(egui_ctx, |ui| {
                    // calculate score
                    ui.label("Congratulations - you won the game");
                    ui.label("During the game...");
                    ui.label(format!("    you gained {} points", self.game.score.earned));
                    ui.label(format!("    and lost {} points", self.game.score.lost));
                    ui.add_space(8.0);
                    ui.label("-----");
                    ui.label(format!("Your final score is {} points", self.game.score.calc()));
                    ui.add_space(20.0);
                    // buttons to quit and restart
                    ui.horizontal(|ui|{
                        // option to start a new game
                        if ui.button("New").clicked() {
                            self.game=Game::new();
                        }
                        ui.add_space(20.0);
                        // option to quit application
                        #[cfg(not(target_arch = "wasm32"))]
                        {
                            if ui.button("Quit").clicked() {
                                std::process::exit(0);
                            }
                        }
                    });
                });

            egui::Window::new("About")
                .open(&mut self.popups.about)
                .show(egui_ctx, |ui| {
                    ui.columns(2, |uis| {
                        uis[0].vertical(|ui| {
                            ui.image(self.icon, egui::Vec2::new(200., 200.));
                            ui.label(&self.name);
                        });
                        uis[1].vertical(|ui| {
                            ui.add_space(20.0);
                            ui.label("Authors: ".to_owned() + env!("CARGO_PKG_AUTHORS"));
                            ui.add_space(5.0);
                            ui.label("Description: ".to_owned() + env!("CARGO_PKG_DESCRIPTION"));
                            ui.add_space(5.0);
                            ui.label("Version: ".to_owned() + env!("CARGO_PKG_VERSION"));
                            ui.add_space(5.0);
                            ui.label(format!("This package is licensed under {}", env!("CARGO_PKG_LICENSE")));
                            ui.add_space(5.0);
                            ui.label("Check out the source code:");
                            ui.hyperlink(env!("CARGO_PKG_REPOSITORY"));
                        });
                    })
                });

            egui::Window::new("Settings")
                .open(&mut self.popups.settings)
                .show(egui_ctx, |ui| {
                    NamedGroup::new("Theme")
                        .show(ui, |ui|{
                            ui.vertical(|ui|{
                                ui.add_space(8.0);
                                ui.label("Changes made here are only persist for this session, unless you save them");
                                ui.add_space(8.0);
                                unsafe {
                                    ui.columns(4, |col|{
                                        col[0].label("Spades");
                                        col[0].color_edit_button_srgb(&mut card::SPADES_COLOR);

                                        col[1].label("Diamonds");
                                        col[1].color_edit_button_srgb(&mut card::DIAMONDS_COLOR);

                                        col[2].label("Hearts");
                                        col[2].color_edit_button_srgb(&mut card::HEARTS_COLOR);

                                        col[3].label("Clubs");
                                        col[3].color_edit_button_srgb(&mut card::CLUBS_COLOR);
                                    });
                                }

                                ui.add_space(8.0);
                                ui.horizontal(|ui|{
                                    if ui.button("Reset to default").clicked() {
                                        card::reset_colors();
                                    }
                                    if ui.button("Save").clicked() {
                                        let storage = &mut quad_storage::STORAGE.lock().unwrap();

                                        unsafe {
                                            storage.set("theme_clubs", &card::CLUBS_COLOR
                                                        .to_vec()
                                                        .iter()
                                                        .map(|d|d.to_string())
                                                        .collect::<Vec<String>>()
                                                        .join(","));
                                            storage.set("theme_spades", &card::SPADES_COLOR
                                                        .to_vec()
                                                        .iter()
                                                        .map(|d|d.to_string())
                                                        .collect::<Vec<String>>()
                                                        .join(","));
                                            storage.set("theme_diamonds", &card::DIAMONDS_COLOR
                                                        .to_vec()
                                                        .iter()
                                                        .map(|d|d.to_string())
                                                        .collect::<Vec<String>>()
                                                        .join(","));
                                            storage.set("theme_hearts", &card::HEARTS_COLOR
                                                        .to_vec()
                                                        .iter()
                                                        .map(|d|d.to_string())
                                                        .collect::<Vec<String>>()
                                                        .join(","));
                                        }
                                    }
                                });
                            });
                        })

                });

            egui::Window::new("Stats")
                .open(&mut self.popups.stats)
                .show(egui_ctx, |ui| {
                    ui.vertical(|ui|{
                        if self.stats.is_empty() {
                            ui.label("You have no saved games - start by playing one");
                        } else {
                            ui.label("Scoreboard:");
                            ui.add_space(8.0);
                        }
                        for (i, score) in self.stats.iter().enumerate() {
                            NamedGroup::new(&(i+1).to_string())
                                .show(ui, |ui| {
                                    ui.vertical(|ui|{
                                        ui.label(&format!("Gained: {}", score.earned));
                                        ui.label(&format!("Lost: {}", score.lost));
                                        ui.label(&format!("Overall: {}", score.calc()));
                                        ui.add_space(4.0);
                                        ui.label(&format!("Skill level: {}", score.calc_skill()));
                                        ui.add_space(8.0);
                                    });
                                });
                        }
                    })
                });
        });
        // Draw things behind egui here
        self.egui_mq.draw(ctx);
        // Draw things in front of egui here
        ctx.commit_frame();
    }

    fn mouse_motion_event(&mut self, x: f32, y: f32) {
        let ctx = get_context();
        self.egui_mq.mouse_motion_event(ctx, x, y);
    }
    fn mouse_wheel_event(&mut self, dx: f32, dy: f32) {
        let ctx = get_context();
        self.egui_mq.mouse_wheel_event(ctx, dx, dy);
    }
    fn mouse_button_down_event(
        &mut self,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        let ctx = get_context();
        self.egui_mq.mouse_button_down_event(ctx, mb, x, y);
    }
    fn mouse_button_up_event(
        &mut self,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        let ctx = get_context();
        self.egui_mq.mouse_button_up_event(ctx, mb, x, y);
    }
    fn char_event(
        &mut self,
        character: char,
        _keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        self.egui_mq.char_event(character);
    }
    fn key_down_event(
        &mut self,
        keycode: mq::KeyCode,
        keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        let ctx = get_context();
        self.egui_mq.key_down_event(ctx, keycode, keymods);
    }
    fn key_up_event(&mut self, keycode: mq::KeyCode, keymods: mq::KeyMods) {
        self.egui_mq.key_up_event(keycode, keymods);
    }
}

fn main() {
    let conf = mq::conf::Conf {
        window_title: "Suitsquad".to_owned(),
        high_dpi: true,
        ..Default::default()
    };
    mq::start(conf, |ctx| {
        unsafe {
            CONTEXT = Some(ctx);
        }
        let ctx = get_context();
        mq::UserData::free(App::new(ctx))
    });
}
