use egui;

pub struct NamedGroup {
    name: String
}
impl NamedGroup {
    pub fn new(name: &str) -> NamedGroup {
        NamedGroup {
            name: String::from(name)
        }
    }

    pub fn show<R>(&self, ui: &mut egui::Ui, add_component: impl FnOnce(&mut egui::Ui) -> R) {
        ui.group(|ui| {
            ui.vertical(|ui| {
                ui.label(&self.name);
                ui.add_space(4.0);
                ui.horizontal(|ui| {
                    add_component(ui);
                });
                ui.add_space(8.0);
            })
        });
    }
}
