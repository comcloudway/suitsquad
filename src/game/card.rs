// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/card.rs
// and modified it to *suit* egui

pub static mut CLUBS_COLOR: [u8;3] = [0x4d, 0x4d, 0x42];
pub static mut SPADES_COLOR:[u8;3] = [0x4d, 0x4d, 0x4d];
pub static mut DIAMONDS_COLOR: [u8;3] = [0xd5, 0x4e, 0x53];
pub static mut HEARTS_COLOR:[u8;3] = [0xd5, 0x43, 0x53];

pub fn reset_colors() {
    unsafe {
        CLUBS_COLOR = [0x4d, 0x4d, 0x42];
        SPADES_COLOR = [0x4d, 0x4d, 0x4d];
        DIAMONDS_COLOR = [0xd5, 0x4e, 0x53];
        HEARTS_COLOR = [0xd5, 0x43, 0x53];
    }
}

/// the type/suit of playing card
#[derive(PartialEq, Clone, Copy)]
pub enum CardSuit {
    Clubs,
    Diamonds,
    Hearts,
    Spades
}
impl CardSuit {
    pub fn default() -> CardSuit {
        CardSuit::Clubs
    }

    pub fn to_color(&self) -> egui::Color32 {
        unsafe {
            match self {
                CardSuit::Clubs => egui::Color32::from_rgb(CLUBS_COLOR[0], CLUBS_COLOR[1], CLUBS_COLOR[2]),
                CardSuit::Spades => egui::Color32::from_rgb(SPADES_COLOR[0], SPADES_COLOR[1], SPADES_COLOR[2]),
                CardSuit::Diamonds => egui::Color32::from_rgb(DIAMONDS_COLOR[0], DIAMONDS_COLOR[1], DIAMONDS_COLOR[2]),
                CardSuit::Hearts => egui::Color32::from_rgb(HEARTS_COLOR[0], HEARTS_COLOR[1], HEARTS_COLOR[2]),
            }
        }
    }

    pub fn to_char(&self) -> char {
        match self {
            CardSuit::Clubs => '♣',
            CardSuit::Diamonds => '♦',
            CardSuit::Hearts => '♥',
            CardSuit::Spades => '♠'
        }
    }
}

/// the card value
#[derive(Clone, Copy)]
pub enum CardValue {
    Ace,
    /// numeric card value 2 to 10
    Number(u8),
    Jack,
    Queen,
    King
}
impl CardValue {
    /// try to create a card from a number
    /// obviously 0<=num<=13
    pub fn try_from_u8(num: u8) -> Option<CardValue> {
        match num {
            1 => Some(CardValue::Ace),
            11 => Some(CardValue::Jack),
            12 => Some(CardValue::Queen),
            13 => Some(CardValue::King),
            n => if n<11 { Some(CardValue::Number(n)) } else { None }
        }
    }
    /// get default value
    pub fn default() -> CardValue {
        CardValue::Ace
    }
    /// returns the numeric card value
    pub fn to_num(&self) -> u8 {
        match self {
            CardValue::Ace => 1,
            CardValue::Number(n) => *n,
            CardValue::Jack => 11,
            CardValue::Queen => 12,
            CardValue::King => 13,
        }
    }

    pub fn to_str(&self) -> String {
        match self {
            CardValue::Ace => String::from("A"),
            CardValue::Number(num)=>num.to_string(),
            CardValue::Jack => String::from("J"),
            CardValue::Queen => String::from("Q"),
            CardValue::King => String::from("K"),
        }
    }
}

pub struct CardDrawContext {
    /// is the card actually there
    /// used to draw an empty stack
    pub card: Option<Card>,
    /// card is the last card in the stack
    /// card is drawn as whole card
    /// not only partially
    pub is_last: bool,
    /// scale the card
    /// by the factor
    /// 1.0 is the default size
    pub scale: f32,
}
impl CardDrawContext {
    pub fn get_width() -> f32 {
        75.
    }
    pub fn get_height() -> f32 {
        110.
    }
}

#[derive(Clone)]
pub struct Card {
    pub suit: CardSuit,
    pub value: CardValue,
    pub visible: bool
}
impl Card {
    /// get default value
    pub fn default() -> Card {
        Card {
            suit: CardSuit::default(),
            value: CardValue::default(),
            visible: false
        }
    }
    /// create a new card
    pub fn new(suit: CardSuit, value: CardValue) -> Card {
        Card {
            suit,
            value,
            ..Card::default()
        }
    }
    /// draws the card
    pub fn draw(ui: &mut egui::Ui, ctx: CardDrawContext) -> (egui::Rect, egui::Response) {
        // configuration for full card
        let width = CardDrawContext::get_width();
        let mut height = CardDrawContext::get_height();
        let radius = 8.;
        let mut stroke = egui::Stroke::new(2., egui::Color32::from_rgb(0x42, 0x42, 0x42));
        let mut gray = 30;

        if !ctx.is_last {
            height /= 4.;
        }

        let (rect, resp) = ui.allocate_at_least(egui::vec2(width * ctx.scale, height * ctx.scale), egui::Sense::click_and_drag());

        if let Some(card) = &ctx.card {
            if card.visible {
                // show value
                stroke = egui::Stroke::new(2., card.suit.to_color());

                gray = 50;
            } else {
                // dont show value
                gray = 40;
                stroke = egui::Stroke::none();
            }

            if resp.hovered() && (ctx.is_last || card.visible) {
                gray -= 5;
            }
        }

        ui.painter().rect(
            rect,
            radius,
            egui::Color32::from_gray(gray),
            stroke
        );

        if let Some(card) = &ctx.card {
            if card.visible {
                ui.painter().with_clip_rect(rect)
                            .text(
                                rect.center(),
                                egui::Align2::CENTER_CENTER,
                                format!("{} {}", card.suit.to_char(), card.value.to_str()),
                                egui::FontId::default(),
                                stroke.color
                            );

            }
        }

        (rect, resp)

    }
}
