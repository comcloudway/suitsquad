use serde::{Deserialize, Serialize};
// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/score.rs
// and modified it to *suit* egui

/// stores the players score
#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct Score {
    /// points earned
    pub earned: usize,
    /// points lost
    pub lost: usize,
    /// count the moves
    pub moves: usize
}
impl Score {
    /// returns the default score config
    pub fn default() -> Score {
        Score {
            earned: 0,
            lost: 0,
            moves: 0
        }
    }

    /// calculates the final score
    pub fn calc(&self) -> isize {
        self.earned as isize - self.lost as isize
    }

    pub fn calc_skill(&self) -> f32 {
        self.calc() as f32 / self.moves as f32
    }

    pub fn gain(&mut self, num: usize) {
        self.earned+=num;
    }
    pub fn lose(&mut self, num: usize) {
        self.lost+=num;
    }
    /// add move
    pub fn next(&mut self) {
        self.moves+=1;
    }
}
