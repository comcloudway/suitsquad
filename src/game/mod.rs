pub mod card;
pub mod deck;
pub mod score;
pub mod stock;
pub mod foundation;
pub mod tableau;

pub use deck::Deck;
pub use card::{Card, CardSuit, CardValue, CardDrawContext};
pub use score::Score;
pub use stock::Stock;
pub use foundation::Foundation;
pub use tableau::Tableau;

use crate::components::NamedGroup;

/// ui slot elements
#[derive(Clone, Copy)]
pub enum Slot {
    Waste,
    Stock,
    Foundation(u8),
    Tableau(u8)
}
/// current card move
pub enum MoveOperation {
    /// uncompleted move
    /// still being dragged
    Unsigned {
        content: Vec<Card>,
        from: Slot
    },
    /// completed move
    /// drag ended
    /// ready to be processed
    Signed {
        content: Vec<Card>,
        from: Slot,
        to: Slot
    },
    /// no move
    None
}

// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/foundation.rs
// and modified it to *suit* egui

/// struct to store the game data
pub struct Game {
    /// stock piles
    pub stock: Stock,
    /// store score
    pub score: Score,
    /// store foundation
    pub foundation: Foundation,
    /// store tableau
    pub tableau: Tableau,
    /// move operation data
    pub operation: MoveOperation,
}
impl Game {
    /// start new game
    pub fn new() -> Game {
        let mut g = Game {
            stock: Stock::default(),
            score: Score::default(),
            foundation: Foundation::default(),
            tableau: Tableau::default(),
            operation: MoveOperation::None
        };

        // generate cards
        let mut deck = Deck::new();
        deck.shuffle();

        // assign cards
        for s in 1..8 {
            for c in 0..s {
                if let Some(mut card) = deck.0.pop() {
                    if c==s-1 {
                        card.visible=true;
                    }
                    (match s-1 {
                        0 => &mut g.tableau.slot0,
                        1 => &mut g.tableau.slot1,
                        2 => &mut g.tableau.slot2,
                        3 => &mut g.tableau.slot3,
                        4 => &mut g.tableau.slot4,
                        5 => &mut g.tableau.slot5,
                        _ => &mut g.tableau.slot6
                    }).push(card);
                }
            }
        }

        g.stock.pile=deck.0;

        return g;
    }

    /// check if game is won
    pub fn is_won(&self) -> bool {
        for f in 0..4 {
            if self.foundation.get(f).len() != 13 {
                return false;
            }
        }

        true
    }

    pub fn reset_operation(&mut self) {
        if let MoveOperation::Unsigned {
            from,
            content
        } = &self.operation {
            match from {
                Slot::Foundation(f) => {
                    self.foundation.get_mut(*f).append(&mut content.clone());
                    self.operation=MoveOperation::None;
                },
                Slot::Stock => {
                    self.operation=MoveOperation::None;
                },
                Slot::Waste => {
                    self.stock.waste.append(&mut content.clone());
                    self.operation=MoveOperation::None;
                },
                Slot::Tableau(t) => {
                    self.tableau.get_mut(*t).append(&mut content.clone());
                    self.operation=MoveOperation::None;
                }
            }
        }
        if let MoveOperation::Signed {
            from,
            content,
            ..
        } = &self.operation {
            match from {
                Slot::Foundation(f) => {
                    self.foundation.get_mut(*f).append(&mut content.clone());
                    self.operation=MoveOperation::None;
                },
                Slot::Stock => {
                    self.operation=MoveOperation::None;
                },
                Slot::Waste => {
                    self.stock.waste.append(&mut content.clone());
                    self.operation=MoveOperation::None;
                },
                Slot::Tableau(t) => {
                    self.tableau.get_mut(*t).append(&mut content.clone());
                    self.operation=MoveOperation::None;
                }
            }
        }
    }

    pub fn draw(&mut self, ui: &mut egui::Ui) {
        ui.vertical(|ui| {
            // top row
            ui.horizontal(|ui| {
                // stock
                NamedGroup::new("Stock")
                    .show(ui, |ui| {
                        ui.add_space(8.0);
                        // stock pile
                        {
                            let ctx = CardDrawContext {
                                is_last: true,
                                card: if let Some(c) = self.stock.pile.last() { Some(c.clone()) } else { None },
                                scale: crate::get_card_scale()
                            };
                            let (_, resp) = Card::draw(ui, ctx);
                            if resp.clicked() {
                                if !self.stock.next() {
                                    self.score.lost=self.score.lost+100;
                                }
                                self.score.next();
                            }
                        }
                        ui.add_space(8.0);
                        // waste pile
                        {
                            let ctx = CardDrawContext {
                                is_last: true,
                                card: if let Some(c) = self.stock.waste.last() { Some(c.clone()) } else { None },
                                scale: crate::get_card_scale()
                            };
                            let (_, resp) = Card::draw(ui, ctx);
                            if resp.clicked() {
                                match self.operation {
                                    MoveOperation::None => {
                                        // create unsigned operation
                                        if let Some(card) = self.stock.waste.pop() {
                                            self.operation=MoveOperation::Unsigned {
                                                from: Slot::Waste,
                                                content: vec![card]
                                            };
                                        }
                                    },
                                    MoveOperation::Unsigned{..} | MoveOperation::Signed{..} => {
                                        // reset operation
                                        // impossible move
                                        self.reset_operation();
                                    }
                                }
                            }
                        }
                        ui.add_space(4.0);
                    });
                ui.add_space(62.0 * crate::get_card_scale());

                // foundation
                NamedGroup::new("Foundation")
                    .show(ui, |ui| {
                        ui.add_space(8.0);
                        for slot in 0..4 {
                            let ctx = CardDrawContext {
                                is_last: true,
                                card: if let Some(c) = self.foundation.get(slot).last() { Some(c.clone()) } else { None },
                                scale: crate::get_card_scale()
                            };
                            let (_, resp) = Card::draw(ui, ctx);
                            if resp.clicked() {
                                match &self.operation {
                                    MoveOperation::None => {
                                        self.operation=MoveOperation::Unsigned {
                                            from: Slot::Foundation(slot),
                                            content: if let Some(card) = self.foundation.get_mut(slot).pop() { vec![card] } else {Vec::new()}
                                        };
                                    },
                                    MoveOperation::Unsigned {
                                        from,
                                        content
                                    } => {
                                        self.operation=MoveOperation::Signed {
                                            from: *from,
                                            content: content.to_vec(),
                                            to: Slot::Foundation(slot)
                                        }
                                    },
                                    MoveOperation::Signed{..} => {
                                        self.reset_operation();
                                    }
                                }
                            }
                            ui.add_space(4.0);
                        }
                    });
            });
            ui.add_space(20.0);
            // bottom row
            // tableau
            NamedGroup::new("Tableau")
                .show(ui, |ui| {
                    ui.add_space(8.0);
                    for slot in 0..7 {
                        ui.vertical(|ui| {
                            if self.tableau.get(slot).is_empty() {
                                let ctx = CardDrawContext {
                                    is_last: true,
                                    card: None,
                                    scale: crate::get_card_scale()
                                };
                                let (_, resp) = Card::draw(ui, ctx);
                                if resp.clicked() {
                                    match &self.operation {
                                        MoveOperation::None => {
                                            self.reset_operation();
                                        },
                                        MoveOperation::Unsigned {
                                            from,
                                            content
                                        } => {
                                            // sign move operation
                                            if let Some(card) = content.first() {
                                                // validate move
                                                if self.tableau.is_valid(card, slot) {
                                                    self.operation = MoveOperation::Signed {
                                                        from: *from,
                                                        content: content.to_vec(),
                                                        to: Slot::Tableau(slot)
                                                    }
                                                } else {
                                                    self.reset_operation();
                                                }
                                            } else {
                                                self.reset_operation();
                                            }
                                        },
                                        MoveOperation::Signed{..} => {
                                            self.reset_operation();
                                        }
                                    }
                                }
                            }
                            for (i, card) in self.tableau.get(slot).clone().iter().enumerate() {
                                let ctx = CardDrawContext {
                                    is_last: i+1 == self.tableau.get(slot).len(),
                                    card: Some(card.clone()),
                                    scale: crate::get_card_scale()
                                };
                                let (_, resp) = Card::draw(ui, ctx);
                                ui.add_space(3.0);

                                if resp.clicked() {
                                    match &self.operation {
                                        MoveOperation::None => {
                                            // move from to operation
                                            // from here
                                            // create operation
                                            if card.visible {
                                                self.operation = MoveOperation::Unsigned {
                                                    from: Slot::Tableau(slot),
                                                    content: self.tableau.get_mut(slot).split_off(i)
                                                };
                                            } else if i+1 == self.tableau.get(slot).len() {
                                                if let Some(card) = self.tableau.get_mut(slot).get_mut(i) {
                                                    card.visible=true;
                                                    self.score.gain(5);
                                                }
                                                self.reset_operation();
                                            } else {
                                                self.reset_operation();
                                            }
                                        },
                                        MoveOperation::Unsigned {
                                            from,
                                            content
                                        } => {
                                            // sign move operation
                                            if let Some(card) = content.first() {
                                                // validate move
                                                if self.tableau.is_valid(card, slot) {
                                                    self.operation = MoveOperation::Signed {
                                                        from: *from,
                                                        content: content.to_vec(),
                                                        to: Slot::Tableau(slot)
                                                    }
                                                } else {
                                                    self.reset_operation();
                                                }
                                            } else {
                                                self.reset_operation();
                                            }
                                        },
                                        MoveOperation::Signed{..} => {
                                            self.reset_operation();
                                        }
                                    }
                                }
                            }
                        });
                        ui.add_space(4.0);
                    }
                });
            ui.add_space(20.0);

            // score row
            NamedGroup::new("Score")
                .show(ui, |ui| {
                    ui.label(format!("Current score: {} ({}/-{})", self.score.calc(), self.score.earned, self.score.lost));
                    ui.label(format!("Move count: {}", self.score.moves));
                });

        });
    }

    /// process move opertion
    pub fn tick(&mut self) {
        if let MoveOperation::Signed {
            from,
            to,
            content
        } = &self.operation {
            match to {
                Slot::Waste | Slot::Stock => {
                    self.reset_operation();
                },
                Slot::Tableau(t) => {
                    if let Some(card) = content.first() {
                        if self.tableau.is_valid(card, *t) {
                            self.tableau.get_mut(*t).append(&mut content.clone());

                            match from {
                                Slot::Waste => self.score.gain(5),
                                Slot::Foundation(_) => self.score.lose(15),
                                _ => ()
                            };

                            self.score.next();
                            self.operation=MoveOperation::None;
                            return;
                        }
                    }
                    self.reset_operation();
                },
                Slot::Foundation(f) => {
                    if let Some(card) = content.first() {
                        if content.len() == 1 && self.foundation.is_valid(card, *f) {
                            self.foundation.get_mut(*f).append(&mut content.clone());

                            match from {
                                Slot::Waste => self.score.gain(10),
                                Slot::Tableau(_) => self.score.gain(10),
                                _ => ()
                            }

                            self.score.next();
                            self.operation=MoveOperation::None;
                            return;
                        }
                    }
                    self.reset_operation();
                }
            }
        }
    }
}
