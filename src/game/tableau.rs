// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/score.rs
// and modified it to *suit* egui

use crate::game::{Card, CardSuit, CardValue};

/// represents table
pub struct Tableau {
    pub slot0: Vec<Card>,
    pub slot1: Vec<Card>,
    pub slot2: Vec<Card>,
    pub slot3: Vec<Card>,
    pub slot4: Vec<Card>,
    pub slot5: Vec<Card>,
    pub slot6: Vec<Card>,
}
impl Tableau {
    /// create empty tableau
    pub fn default() -> Tableau {
        Tableau {
            slot0: Vec::new(),
            slot1: Vec::new(),
            slot2: Vec::new(),
            slot3: Vec::new(),
            slot4: Vec::new(),
            slot5: Vec::new(),
            slot6: Vec::new(),
        }
    }

    /// returns a non-mutable reference to a numeric slot
    pub fn get(&self, slot:u8) -> &Vec<Card> {
        match slot {
            0 => &self.slot0,
            1 => &self.slot1,
            2 => &self.slot2,
            3 => &self.slot3,
            4 => &self.slot4,
            5 => &self.slot5,
            _ => &self.slot6
        }
    }

    /// returns a mutable reference to a numeric slot
    pub fn get_mut(&mut self, slot:u8) -> &mut Vec<Card> {
        match slot {
            0 => &mut self.slot0,
            1 => &mut self.slot1,
            2 => &mut self.slot2,
            3 => &mut self.slot3,
            4 => &mut self.slot4,
            5 => &mut self.slot5,
            _ => &mut self.slot6
        }
    }

    /// validate a move
    /// return true if allows
    pub fn is_valid(&self, card: &Card, slot: u8) -> bool {
        // invalid slot
        if slot > 6 {
            return false;
        }

        let s = self.get(slot);
        if s.is_empty() {
            // card to be king
            if let CardValue::King = card.value {
                return true;
            } else {
                return false;
            }
        } else {
            // card has to be of opposing color and a higher numeric value
            let v = card.value.to_num();
            let last: &Card = s.last().unwrap();

            if last.value.to_num() == v+1 {
                return match card.suit {
                    CardSuit::Clubs | CardSuit::Spades => match last.suit {
                        CardSuit::Clubs | CardSuit::Spades => false,
                        _ => true
                    },
                    CardSuit::Diamonds | CardSuit::Hearts => match last.suit {
                        CardSuit::Diamonds | CardSuit::Hearts => false,
                        _ => true
                    },
                }

            }
            return false
        }
    }
}
