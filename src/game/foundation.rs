// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/foundation.rs
// and modified it to *suit* egui

use crate::game::{Card, CardValue};

/// represents the foundation window
pub struct Foundation {
    pub slot0: Vec<Card>,
    pub slot1: Vec<Card>,
    pub slot2: Vec<Card>,
    pub slot3: Vec<Card>,
}
impl Foundation {
    /// initialised foundation object
    pub fn default() -> Foundation {
        Foundation {
            slot0: Vec::new(),
            slot1: Vec::new(),
            slot2: Vec::new(),
            slot3: Vec::new(),
        }
    }

    /// checks if the card can be added to the stack
    /// returns true if move is possible
    pub fn is_valid(&self, card: &Card, slot: u8) -> bool {
        if slot>3 {
            return false;
        } else {
            let v = match slot {
                0=>&self.slot0,
                1=>&self.slot1,
                2=>&self.slot2,
                _=>&self.slot3,
            };

            if v.is_empty() {
                // card has be an ace
                if let CardValue::Ace = card.value {
                    return true;
                } else {
                    return false;
                }
            } else {
                // card as to be of same color as previous card
                // and one number higher
                let last:&Card = v.last().unwrap();
                if card.value.to_num() != last.value.to_num()+1 {
                    return false;
                }

                if last.suit != card.suit {
                    return false;
                }

                return true;
            }
        }
    }

    // get a non mutable reference to the given slot
    pub fn get(&self, slot: u8) -> &Vec<Card> {
        match slot {
            0=>&self.slot0,
            1=>&self.slot1,
            2=>&self.slot2,
            _=>&self.slot3,
        }
    }

    /// gets a mutable reference to the given slot
    pub fn get_mut(&mut self, slot: u8) -> &mut Vec<Card> {
        match slot {
            0=>&mut self.slot0,
            1=>&mut self.slot1,
            2=>&mut self.slot2,
            _=>&mut self.slot3,
        }
    }
}
