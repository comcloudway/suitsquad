// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/deck.rs
// and modified it to *suit* egui

use crate::game::card::{
    Card,
    CardSuit,
    CardValue
};
use std::time::{
    SystemTime,
    UNIX_EPOCH
};
use quad_rand::ChooseRandom;

pub struct Deck(pub Vec<Card>);
impl Deck {
    /// create a new deck including all suits and cards
    pub fn new() -> Deck {
        let mut deck = Vec::new();
        for suit in [CardSuit::Clubs, CardSuit::Diamonds, CardSuit::Hearts, CardSuit::Spades] {
            for value in 1..13+1 {
                deck.push(Card::new(
                    suit,
                    if let Some(n) = CardValue::try_from_u8(value) {n} else {CardValue::Number(1)},
                ));
            }
        }
        return Deck(deck);
    }
    /// mix the given deck
    pub fn shuffle(&mut self) {
        if let Ok(n) = SystemTime::now().duration_since(UNIX_EPOCH) {
            quad_rand::srand(n.as_secs());
        }
        self.0.shuffle();
    }
}
