// this code is based on the code written for my suits game
// https://codeberg.org/comcloudway/suits/src/branch/main/src/game/stock.rs
// and modified it to *suit* egui

use crate::game::Card;

/// stock window
pub struct Stock {
    /// stock pile
    pub pile: Vec<Card>,
    /// turned over cards
    pub waste: Vec<Card>
}
impl Stock {
    /// create new stock instance
    pub fn default() -> Stock {
        Stock {
            pile: Vec::new(),
            waste: Vec::new()
        }
    }

    /// flips next card from stock to waste
    /// return true if there was no need to reset
    pub fn next(&mut self) -> bool {
        if self.pile.is_empty() && !self.waste.is_empty() {
            // flip waste
            for _ in 0..self.waste.len() {
                if let Some(mut card) = self.waste.pop() {
                    card.visible=false;
                    self.pile.push(card);
                }
            }
            return false;
        }
        if !self.pile.is_empty() {
            // flip card
            if let Some(mut card) = self.pile.pop() {
                card.visible=true;
                self.waste.push(card);
            }
        }
        true
    }
}
